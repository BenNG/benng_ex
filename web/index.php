<?php

// web/index.php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/db.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;


use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;


$app = new Silex\Application();


function is_api_key_ok($db, $request)
{
    $keys        = array();
    $res_api_key = $db->query('SELECT key FROM API_KEY');
    
    while ($row_key = $res_api_key->fetchArray(SQLITE3_ASSOC)) {
        array_push($keys, $row_key['key']);
    }
    
    $params  = $request->query->all();
    $api_key = $params['api_key'];
    
    return in_array($api_key, $keys);
}

function bad_api_key_response()
{
    $response = new Response('bad api key');
    $response->setStatusCode(401, 'bad api key');
    return $response;
}

$app->get('/cooltainers/{uuid}', function(Request $request, $uuid) use ($app, $db)
{
    if (is_api_key_ok($db, $request)) {
        $result      = $db->query("SELECT uuid, data FROM cooltainers WHERE uuid='$uuid'");
        $row         = $result->fetchArray(SQLITE3_ASSOC);
        $row['data'] = json_decode($row['data']);
        return $app->json($row, 200);
    }
    return bad_api_key_response();
});

$app->get('/cooltainers', function(Request $request) use ($app, $db)
{
    $result = $db->query('SELECT uuid, data FROM cooltainers');
    
    $myArray = array();
    
    if (is_api_key_ok($db, $request)) {
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $row['data'] = json_decode($row['data']);
            array_push($myArray, $row);
        }
        return $app->json($myArray, 200);
    }
    return bad_api_key_response();
    
});

$app->post('/cooltainers', function(Request $request) use ($app, $db)
{
    
    if (is_api_key_ok($db, $request)) {
        // need to implement some controls here
        $raw     = $request->getContent();
        $decoded = json_decode($raw, true);
        $data    = $decoded['data'];
        $encoded = json_encode($data);
        
        $id = Uuid::uuid4()->toString();
        $db->exec("INSERT INTO cooltainers (uuid, data) VALUES ('$id', '$encoded')");
        
        $cooltainer = array(
        'uuid' => $id,
        'data' => json_decode($encoded)
        );
        
        return $app->json($cooltainer, 201);
    }
    return bad_api_key_response();
    
});

$app->run();