<?php

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

function sqlite_table_exists($sqlite, $table)
{
    $result = $sqlite->query("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='$table'");
    $num    = $result->fetchArray(SQLITE3_NUM);
    // 	var_dump($num[0] != 0);
    return $num[0] != 0;
}

class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('mysqlitedb.db');
    }
}

$db = new MyDB();

if (!$db) {
    echo $db->lastErrorMsg();
} else {
    
    if (sqlite_table_exists($db, "cooltainers") == FALSE) {
        // 		echo "Opened database successfully\n";
        // 		$db->exec('DROP TABLE IF EXISTS cooltainers');
        $db->exec('CREATE TABLE cooltainers (uuid STRING  PRIMARY KEY, data STRING)');
        $db->exec('CREATE TABLE API_KEY (uuid STRING  PRIMARY KEY, key STRING)');
        
        $id1   = Uuid::uuid4()->toString();
        $data1 = '{"datetime":"2017-03-08T10:51:59.653Z","temperature":25,"light":1000,"water":{"k":0.01,"n":0.01,"p":0.01},"air":{"co2":0.01}}';
        $db->exec("INSERT INTO cooltainers (uuid, data) VALUES ('$id1', '$data1')");
        
        $id2   = Uuid::uuid4()->toString();
        $data2 = '{"datetime":"2017-03-08T10:51:59.653Z","temperature":26,"light":2000,"water":{"k":0.02,"n":0.02,"p":0.02},"air":{"co2":0.02}}';
        $db->exec("INSERT INTO cooltainers (uuid, data) VALUES ('$id2', '$data2')");
        
        $id3   = Uuid::uuid4()->toString();
        $data3 = '{"datetime":"2017-03-08T10:51:59.653Z","temperature":27,"light":3000,"water":{"k":0.03,"n":0.03,"p":0.03},"air":{"co2":0.03}}';
        $db->exec("INSERT INTO cooltainers (uuid, data) VALUES ('$id3', '$data3')");
        
        $id4 = Uuid::uuid4()->toString();
        $db->exec("INSERT INTO API_KEY (uuid, key) VALUES ('$id4', '123abc')");
        
        $id5 = Uuid::uuid4()->toString();
        $db->exec("INSERT INTO API_KEY (uuid, key) VALUES ('$id5', 'xxxwww')");
        
        $id6 = Uuid::uuid4()->toString();
        $db->exec("INSERT INTO API_KEY (uuid, key) VALUES ('$id6', '999zzz')");
        
    }
    
}