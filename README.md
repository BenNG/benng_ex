# My project's README


## Tips
- La base de données est sqlite3 (mysqlitedb.db). J'ai du installer ceci.
```
sudo apt-get install php7.0-sqlite3
```
- Au démarrage du server si mysqlitedb.db le fichier est créé.
- les clefs api sont dans la table API_KEY voir db.php

## Php version
```
➜  benng_ex git:(master) php --version
PHP 7.0.15-0ubuntu0.16.04.4 (cli) ( NTS )
Copyright (c) 1997-2017 The PHP Group
Zend Engine v3.0.0, Copyright (c) 1998-2017 Zend Technologies
    with Zend OPcache v7.0.15-0ubuntu0.16.04.4, Copyright (c) 1999-2017, by Zend Technologies
```

## How to run ?
```
php installer
php composer.phar install
php -S localhost:8080 -t web web/index.php
```

## Curl commands
```
curl 'localhost:8080/cooltainers?api_key=999zzz'
curl 'localhost:8080/cooltainers/your-cooltainer-id?api_key=999zzz'
curl -X POST 'localhost:8080/cooltainers?api_key=999zzz' --data-binary "@data/new_cooltainer.json" -H 'Content-Type: application/json'
```

## [httpie](https://github.com/jkbrzt/httpie) commands
```
http -hb http://localhost:8080/cooltainers api_key==999zzz # without /
http -hb http://localhost:8080/cooltainers/your-cooltainer-id api_key==999zzz
http http://localhost:8080/cooltainers api_key==999zzz < data/new_cooltainer.json
```

## Choix du serveur (de manière simple)

J'ai consulté ceci pour avoir une vague idée [php-framework-benchmark](https://github.com/kenjis/php-framework-benchmark).

Partons sur une architecture qui nous permettra de tenir un bout de temps. 10 000 cooltainers. Ces 10 000 cooltainers vont nous envoyer des infos dans un laps de temps d'une minute: 
```
10 000 cooltainers / 60s -> 166 disons 200 req/s
```
Lors de ma création de cooltainer j'envoie ~150 octects disons 200.
faisons le calcul:
```
200 * 200 = 40 000 octects soit 40 ko/s
40 * 3600 = 144 444 ko/min soit 145 mo/h
-> 3456 mo/j -> ~ 104 go/mois
```

Sur digital ocean par example, la plus petite instance que l'on peut choisir (5$) à 1To donc on est large.  
Par contre il va falloir se méfier de l'espace disque en fonction de ce qu'on veut faire avec les données.  
Selon la disponibilté qu'il nous faut, et [des instances dispo](https://www.digitalocean.com/pricing/#droplet) je propose:

- 2 load balancer
    - instance à 10 voir à 5 en fonction de l'OS
- 2 servers d'applications
    - instance à 20 trop limite à 10
- 2 server de BDD un passif un actif
    - instance à 40 en fonction de ce que l'on veut faire avec les données

140€ par mois. Je pense que c'est une architecture standard mais je ne suis pas spécialiste dans le domaine.

Après le 10 000 est peut être exagéré mais au final ça ne change pas grand chose entre 10 000 et 500. Si la dispo est importante il va falloir quand même répliquer.

## Provisionning

Après avoir tenté d'apprendre [Chef](https://www.chef.io/chef/) qui est une véritable usine, je suis tombé sur 
[Ansible](https://github.com/ansible/ansible).  
C'est un outil qui permet d'automatiser ses déploiements.  
A la différence de chef et des autres qui nécessite l'installation d'un client sur l'instance, le prérequis d'Ansible est uniquement d'avoir un access ssh sur la machine.

Dans notre cas on pourrait:

- créer un user (si pas déjà fait)
- installer php, git, nginx, ...
- copier des informations de manière sécurisée (clef ssh de deploiement)
- git cloner l'application avec la clef ssh
- mettre à jour la config nginx (si pas déjà fait)
- restarter nginx

C'est en gros ce que je fais pour mettre à jour mon [cv](http://benng.info) !

## dev tips

- test sqlite requests with CLI
```
sqlite3 mysqlitedb.db

SELECT count(*) FROM sqlite_master WHERE type='table' AND name='cooltainers';
SELECT * FROM cooltainers;

```