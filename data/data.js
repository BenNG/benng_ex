var existingCooltainers = [
    {
        "id": "uuid1",
        "datetime": "2017-03-08T10:51:59.653Z",
        "temperature": 25,
        "light": 1000,
        "water": {
            "k": 0.01,
            "n": 0.01,
            "p": 0.01
        },
        "air": {
            "co2": 0.01
        }
    },
    {
        "id": "uuid2",
        "datetime": "2017-03-08T10:51:59.653Z",
        "temperature": 26,
        "light": 2000,
        "water": {
            "k": 0.02,
            "n": 0.02,
            "p": 0.02
        },
        "air": {
            "co2": 0.02
        }
    },
    {
        "id": "uuid3",
        "datetime": "2017-03-08T10:51:59.653Z",
        "temperature": 27,
        "light": 3000,
        "water": {
            "k": 0.03,
            "n": 0.03,
            "p": 0.03
        },
        "air": {
            "co2": 0.03
        }
    }
]